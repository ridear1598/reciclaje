import 'package:flutter/material.dart';
import 'dart:math' as math;
class Responsive{
  double _ancho;
  double _alto;
  double _diagonal;

double get width=>_ancho;
double get  height=>_alto;
double get diagonal=>_diagonal;

static Responsive of(BuildContext context)=>Responsive(context);

  Responsive(BuildContext context){
    final Size size=MediaQuery.of(context).size;
    this._ancho=size.width;
    this._alto=size.height;

    //Teorema de pitagoras para calcular la diagonal
    // c2+a2+b2=>c=srt(a2+b2)
    this._diagonal=math.sqrt(math.pow(_ancho,2)+math.pow(_alto,2));
  }

  double wp(double porcent)=>_ancho*porcent/100;
  double hp(double porcent)=>_alto*porcent/100;
  double dp(double porcent)=>_diagonal*porcent/100;

}