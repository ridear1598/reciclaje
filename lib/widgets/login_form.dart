import 'package:flutter/material.dart';
import 'package:reciclaje/utils/responsive.dart';

import 'input_text.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    final Responsive responsive= Responsive.of(context);
    
    return Positioned(
            bottom: 30,
            left:20,
            right:20,
            child: Column(
            children:<Widget> [
             InputText(
               label: "Email",
               keyboardType:TextInputType.emailAddress,
               fontSize: responsive.dp(1.4),
            ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color:Colors.black12
                    )  
                  )
                  ),
                child: Row(
                  children:<Widget> [
                    Expanded(
                      child: InputText(
                        label: "Password",
                        esPassword:true,
                        borderEnabled: false,  
                         fontSize: responsive.dp(1.4),
                        ),
                    ),
                   
                     FlatButton(
                        onPressed: (){},
                         padding: EdgeInsets.symmetric(vertical: 10),
                         child: Text('Olvide mi contraseña', style:TextStyle(fontWeight: FontWeight.bold))
                      ),
                  
                  ],
                ),
              ),
              SizedBox(height:responsive.dp(3)),
              SizedBox(
                width: double.infinity,
                child: FlatButton(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  onPressed: (){},
                   child: Text(
                     "Sign in",
                      style: TextStyle(color: Colors.white,fontSize:responsive.dp(1.6))
                   ),
                   color: Colors.pink
                ),
              ),
               SizedBox(height:responsive.dp(2)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:<Widget> [
                  Text('No tienes cuenta?',style:TextStyle(fontSize:responsive.dp(1.6))),
                    FlatButton(
                      onPressed: (){},
                      child:Text(
                        "Crear cuenta",
                        style: TextStyle(color: Colors.pinkAccent)
                      )
                    ),
              
                ],
              ),
               SizedBox(height:responsive.dp(18)),

        ],
      ),
    );
  }
}