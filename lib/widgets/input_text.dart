import 'package:flutter/material.dart';
import 'package:reciclaje/utils/responsive.dart';


class InputText extends StatelessWidget {
  final String  label;
  final TextInputType keyboardType;
  final bool esPassword,borderEnabled;
  final double fontSize;
  const InputText({Key key, 
                  this.label='', 
                  this.esPassword=false,
                  this.fontSize=15,
                  this.borderEnabled=true,
                  this.keyboardType=TextInputType.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        
    return  TextFormField(
              keyboardType: this.keyboardType, 
              obscureText:this.esPassword,
              style: TextStyle(
                fontSize:this.fontSize
              ),
              decoration: InputDecoration(
                labelText: this.label,
                contentPadding: EdgeInsets.symmetric(vertical: 10),
               // border:this.borderEnabled?null:InputBorder.none,
                enabledBorder:this.borderEnabled?UnderlineInputBorder(
                  borderSide: BorderSide(color:Colors.black12)
                ):InputBorder.none,
                labelStyle: TextStyle(
                    color: Colors.black45,
                    fontWeight: FontWeight.w500
                  )
                ),
            );
  }
}