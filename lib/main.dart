import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:reciclaje/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
       DeviceOrientation.portraitUp,
       DeviceOrientation.portraitDown   
    ]);

    return MaterialApp(
      title: 'Material App',
      home: Homepage()
    );
  }
}